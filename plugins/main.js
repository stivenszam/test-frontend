import Vue from "vue";

// Filtros
const moment = require('moment')
moment.locale('es')
Vue.filter('formatDate', function (date, format) {
    if (!date) return ''
    return moment(date).format(format)
})