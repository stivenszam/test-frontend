# Taller de listar noticias por categoria

_Aplicaciion consulta de tiempo https://newsapi.org/_


## Comenzando 🚀

_Estas instrucciones te permitirán obtener una copia del proyecto en funcionamiento en tu máquina local para propósitos de desarrollo y pruebas._


### Pre-requisitos 📋

* Necesitamos tener instalado **GIT** en nuestras maquinas, lo podemos descargar de desde [AQUI](https://git-scm.com/downloads).
* Muchas ganas de aprender!!
----


## Descargar Repositorio desde terminal :cloud:

_Para descargar la repo desde cualquier terminal, usamos el siguiente comando_

```sh
$ git clone https://github.com/CrissZam/testapp.git
```
----

## Configuración inicial (Puesta en marcha) 🔧

1. Instalar paquetes y plugins del sistema.
   ```sh
    $ yarn install
    ```
3. Iniciar la aplicación en el servidor local.
   ```sh
    $ yarn dev
    ```
----

## Frameworks usados 🛠️

* [nuxt - Vue](https://ionicframework.com/) - Framework usado
* [Pug](https://pugjs.org/language/attributes.html) - Compilador HTML usado
* [Stylus](https://stylus-lang.com/) - Preprocesador Css
* [AJAX]() - Consumo de API


## Autores ✒️

* **Cristian Steven Zambrano** - *Trabajo Inicial* - [stivenszam](https://gitlab.com/stivenszam)




---
⌨️ con ❤️ por [Cristian Zambrano](https://gitlab.com/stivenszam) 😊
